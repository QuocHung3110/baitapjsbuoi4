// Bài 1
/**
- Đầu vào: Ba số ngẫu nhiên
- Các bước xử lý:
  * Gắn giá trị num1, num2, num3 lần lượt từ đầu vào.
  * So sánh các giá trị tìm min, mid, max.
  * Xuất ra min, mid, max.
- Đầu ra: Thứ tự tăng dần của ba số.
 */
function sapxep() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  var min;
  var mid;
  var max;
  if (num1 >= num2 && num1 >= num3) {
    max = num1;
    if (num2 >= num3) {
      mid = num2;
      min = num3;
      document.getElementById(
        "sortUp"
      ).innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;
    } else {
      mid = num3;
      min = num2;
      document.getElementById(
        "sortUp"
      ).innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;
    }
  } else if (num2 >= num1 && num2 >= num3) {
    max = num2;
    if (num1 >= num3) {
      mid = num1;
      min = num3;
      document.getElementById(
        "sortUp"
      ).innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;
    } else {
      mid = num3;
      min = num1;
      document.getElementById(
        "sortUp"
      ).innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;
    }
  } else {
    max = num3;
    if (num1 >= num2) {
      mid = num1;
      min = num2;
      document.getElementById(
        "sortUp"
      ).innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;
    } else {
      mid = num2;
      min = num1;
      document.getElementById(
        "sortUp"
      ).innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;
    }
  }
}
// Bài 2
/**
- Đầu vào: Bố, Mẹ, Anh trai, em gái.
- Các bước xử lý:
  * Gắn giá trị rightPerson ở js từ file html.
  * Dùng switch case cho từng trường hợp và đưa ra lời chào.
- Đầu ra: lời chào theo từng người
 */
function sayHello() {
  const rightPerson = document.getElementById("rightPerson").value;
  switch (rightPerson) {
    case "B":
      document.getElementById(
        "sayHello"
      ).innerHTML = ` Chào Bố! Bố vẫn khỏe chứ? <br /> Các ông bố hãy dành nhiều thời gian cho con, chơi với con, đùa nghịch với con, đọc truyện cho con, nói chuyện với con và cùng con làm những việc đơn giản. Ngoài việc gắn kết tình cảm cha con, còn giúp con phát triển toàn diện cả về thể chất lẫn tinh thần.`;
      break;
    case "M":
      document.getElementById(
        "sayHello"
      ).innerHTML = ` Chào Mẹ! Mẹ vẫn khỏe chứ? <br /> Chúc mẹ có luôn vui vẻ và tươi trẻ mẹ nhé! `;
      break;
    case "A":
      document.getElementById(
        "sayHello"
      ).innerHTML = ` Ái chà chà, lại là Con trai cưng của gia đình đây rồi! Anh vẫn khỏe chứ? <br /> Hơn 20 tuổi đầu, khi ở nhà mình đừng làm em bé nữa nghen.`;
      break;
    case "E":
      document.getElementById(
        "sayHello"
      ).innerHTML = ` Ái chà chà, lại là Con gái rượu của gia đình đây rồi! Em vẫn khỏe chứ? <br /> Bản thân con gái rượu thì chỉ gặp con trai cưng. Chúc em bé một đời như ý nhóe! <3`;
      break;
  }
}
// Bài 3
/**
- Đầu vào: 3 số nguyên.
- các bước xử lý:
  * Gán giá trị cho từng số num1, num2, num3.
  * Áp dụng hàm Number.isInteger(value) kiểm tra xem có phải số nguyên không.
  *  -> không phải -> đưa ra cảnh báo.
  *  -> phải -> xét các trường hợp.
- Đầu ra: Bao nhiêu số chẵn, bao nhiêu số lẻ.
 */
function evenOdd() {
  var num1 = document.getElementById("numN1").value * 1;
  console.log("🚀 ~ file: index.js:88 ~ evenOdd ~ num1", num1);
  var num2 = document.getElementById("numN2").value * 1;
  console.log("🚀 ~ file: index.js:90 ~ evenOdd ~ num2", num2);
  var num3 = document.getElementById("numN3").value * 1;
  console.log("🚀 ~ file: index.js:92 ~ evenOdd ~ num3", num3);

  if (
    Number.isInteger(num1) &&
    Number.isInteger(num2) &&
    Number.isInteger(num3)
  ) {
    if (num1 % 2 == 0 && num2 % 2 == 0 && num3 % 2 == 0) {
      document.getElementById(
        "evenOdd"
      ).innerHTML = ` Cả ba số đều là số chẵn.`;
    } else if (
      (num1 % 2 == 0 && num2 % 2 == 0 && num3 % 2 != 0) ||
      (num1 % 2 == 0 && num2 % 2 != 0 && num3 % 2 == 0) ||
      (num1 % 2 != 0 && num2 % 2 == 0 && num3 % 2 == 0)
    ) {
      document.getElementById(
        "evenOdd"
      ).innerHTML = ` Có hai số chẵn và một số lẻ.`;
    } else if (
      (num1 % 2 != 0 && num2 % 2 == 0 && num3 % 2 != 0) ||
      (num1 % 2 != 0 && num2 % 2 != 0 && num3 % 2 == 0) ||
      (num1 % 2 == 0 && num2 % 2 != 0 && num3 % 2 != 0)
    ) {
      document.getElementById(
        "evenOdd"
      ).innerHTML = ` Có một số chẵn và hai số lẻ.`;
    } else {
      document.getElementById("evenOdd").innerHTML = ` Cả ba số đều là số lẻ`;
    }
  } else {
    alert("Vui lòng nhập số nguyên!!!");
  }
}
// Bài 4
/**
- Đầu vào: 3 cạnh của hình tam giác
- Các bước xử lý: 
  * Gắn giá trị cho từng cạnh edge1, edge2, edge3.
  * Kiểm tra tính tồn tại của tam giác:
 *      (1) Tổng hai cạnh không được nhỏ hơn cạnh còn lại. 
        (2) Giá trị các cạnh phải là số dương
 *      -> không thỏa -> nhập lại;
        -> thỏa mãn -> xét các trường hợp.
- Đầu ra: Loại tam giác
 */
function defineTriangle() {
  var edge1 = document.getElementById("edge1").value * 1;
  var edge2 = document.getElementById("edge2").value * 1;
  var edge3 = document.getElementById("edge3").value * 1;
  if (
    edge1 >= edge2 + edge3 ||
    edge2 >= edge1 + edge3 ||
    edge3 >= edge1 + edge2 ||
    edge1 == 0 ||
    edge2 == 0 ||
    edge3 == 0
  ) {
    alert(`Không đủ điều kiện hình thành một tam giác
            (1) Tổng hai cạnh không được nhỏ hơn cạnh còn lại. 
            (2) Giá trị các cạnh phải là số dương
                --> Vui lòng nhập lại giá trị mới`);
  } else {
    if (edge1 == edge2 && edge1 == edge3) {
      document.getElementById("defineTriangle").innerHTML = `Tam giác đều`;
    } else if (edge1 == edge2 || edge1 == edge3 || edge2 == edge3) {
      document.getElementById("defineTriangle").innerHTML = `Tam giác cân`;
    } else if (
      Math.pow(edge1, 2) == Math.pow(edge2, 2) + Math.pow(edge3, 2) ||
      Math.pow(edge2, 2) == Math.pow(edge1, 2) + Math.pow(edge3, 2) ||
      Math.pow(edge3, 2) == Math.pow(edge1, 2) + Math.pow(edge2, 2)
    ) {
      document.getElementById("defineTriangle").innerHTML = `Tam giác vuông`;
    } else {
      document.getElementById("defineTriangle").innerHTML = `Tam giác thường`;
    }
  }
}
